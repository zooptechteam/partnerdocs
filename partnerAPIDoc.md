**API WORKFLOW :**
-------------------

**1. Partner Authorization:**

Every partner will be able to access Zoop API using the auth token. This Auth token needs to be passed as a header with every API request made by partner client App/web.  The Auth token will be sent separately Via eMail.   


**2. Order Booking Process :**

In order to book order from client app. User has to perform search by PNR or search by train, So we have an API 
     
 1. **Book By PNR :**
 
       - User has to enter a valid PNR as an input and in response we will list out all the restaurant which are active & serving food on the given train route falling in between users source and destination. 
        
 2. **Book By Train Number :**

       - Partner Client App Needs to send us train number & route against the given train.
       - We will send the the list of all the restaurant who will be serving on the given train route of the concerned train, as a response. 
       
 3. **Validate PNR :** If User has come from Step 2    
     
        - If user from the client app uses book ny train option. We need to validate the users order cart which he has prepared using train number.
        - User need to enter valid PNR here, we check that the user has booked the order against valid train number in which s/he will be traveling
        - If PNR validation happens successfully. We respond back with the list of restaurant which are serving on this train route.
        
 4. **Push Order**: 
    
        - Once the user checks out from the cart after the payment, client app needs to punch/push order details along with partner transaction id.  
        - In response we will be giving back zoop order Id and IRCTC order ID, against the Partner transaction ID. 
        
**- Push order API structure need to be discussed with the partner before finalizing it. **
    

**API Request-Response Details:**
----------------------------------

**- API for getting list of cuisines:**

**https://&lt;zoop-base-url>/cuisines**

**Example :**
https://partners.staging.zoopindia.in/cuisines

**Request Method :**
GET

**Parameters :**
N/A

**Request Body:**

N/A

**Response :**

```
{
    "status": true,
    "code": 200,
    "message": "Success.",
    "error": "",
    "data": [
        {
            "cuisineId": 1,
            "cuisineName": "South Indian",
            "status": 1
        },
        {
            "cuisineId": 2,
            "cuisineName": "Punjabi",
            "status": 1
        },
        {
            "cuisineId": 3,
            "cuisineName": "North Indian",
            "status": 1
        },
        {
            "cuisineId": 4,
            "cuisineName": "Mughalai",
            "status": 1
        },
        {
            "cuisineId": 5,
            "cuisineName": "Bengali",
            "status": 1
        },
        {
            "cuisineId": 6,
            "cuisineName": "Goan",
            "status": 1
        },
        {
            "cuisineId": 7,
            "cuisineName": "Tamil",
            "status": 1
        },
        {
            "cuisineId": 8,
            "cuisineName": "Andhra",
            "status": 1
        },
        {
            "cuisineId": 9,
            "cuisineName": "Kerala",
            "status": 1
        },
        {
            "cuisineId": 10,
            "cuisineName": "Indian Chinese",
            "status": 1
        },
        {
            "cuisineId": 11,
            "cuisineName": "Chinese",
            "status": 1
        },
        {
            "cuisineId": 12,
            "cuisineName": "Awadhi",
            "status": 1
        },
        {
            "cuisineId": 13,
            "cuisineName": "Malaysian",
            "status": 1
        },
        {
            "cuisineId": 14,
            "cuisineName": "Maharashtrian",
            "status": 1
        },
        {
            "cuisineId": 15,
            "cuisineName": "Tibetan",
            "status": 1
        },
        {
            "cuisineId": 16,
            "cuisineName": "Sri Lankan",
            "status": 1
        },
        {
            "cuisineId": 17,
            "cuisineName": "Sikkimese",
            "status": 1
        },
        {
            "cuisineId": 18,
            "cuisineName": "Pizza",
            "status": 1
        },
        {
            "cuisineId": 19,
            "cuisineName": "Fast Food",
            "status": 1
        },
        {
            "cuisineId": 20,
            "cuisineName": "Taste Of Bihar",
            "status": 1
        },
        {
            "cuisineId": 21,
            "cuisineName": "Assamese",
            "status": 1
        },
        {
            "cuisineId": 22,
            "cuisineName": "Bakery Confectionery",
            "status": 1
        },
        {
            "cuisineId": 23,
            "cuisineName": "Continental",
            "status": 1
        },
        {
            "cuisineId": 24,
            "cuisineName": "Italian",
            "status": 1
        },
        {
            "cuisineId": 25,
            "cuisineName": "Mexican",
            "status": 1
        },
        {
            "cuisineId": 26,
            "cuisineName": "LEBANESE",
            "status": 1
        }
    ]
}
```

Authentication Error :

```
{
    "status": false,
    "code": 401,
    "message": "",
    "error": "You are not authorised."
}
```

**- API for getting list of serving stations with outlets and their menus (search by train):**

**https://&lt;zoop-base-url>/search/train**

Example :
https://partners.staging.zoopindia.in/search/train

Request Method :
POST

Parameters :
valid string of length 5 (train number)

**Request Body:**

{searchString : 12137}

**Response :**

```
{
   "status": true,
   "code": 200,
   "message": "Success.",
   "error": "",
   "data": {
       "trainDetails": {
           "trainId": 1051,
           "trainNumber": "12926",
           "trainName": "PASCHIM EXPRESS",
           "status": 1
       },
       "trainRoutes": [
           {
               "trainId": 1051,
               "stationId": 1842,
               "arrivalTime": "19:10:00",
               "departureTime": "19:15:00",
               "haltTime": "00:05:00",
               "stationCode": "MTJ",
               "stationName": "MATHURA JN",
               "stationImage": "https://zoop.s3.amazonaws.com/station/MTJ.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T103736Z&X-Amz-Expires=30000&X-Amz-Signature=1b6a2a113eb3b454cafa2a75e9ddec63da42fe29a4db15ac712564f618feab69&X-Amz-SignedHeaders=host",
               "expectedTime": "19:10:00",
               "isVisible": true,
               "arrival": null,
               "departure": null,
               "halt": null,
               "arrDate": null,
               "delayArrival": null,
               "schArrivalTime": null,
               "schArrivalDate": null,
               "outlets": [
                   {
                       "outletId": 38,
                       "outletName": "Wah Ji Wah",
                       "isPureVeg": 1,
                       "outletRating": 4.6,
                       "outletImage": "https://zoop.s3.amazonaws.com/outlets-images/251.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T103736Z&X-Amz-Expires=30000&X-Amz-Signature=814733b25f8e5f55757efca28d4d8b7cf8ba2206e434add4a32d119d0457e34e&X-Amz-SignedHeaders=host",
                       "weeklyOff": "noOff",
                       "gstin": "09AABPF1372P1ZJ",
                       "zoopStatus": 1,
                       "fssaiNo": "22717811000050",
                       "fssaiExpiryDate": "2022-04-26T18:30:00.000Z",
                       "minimumOrderValue": 125,
                       "openTime": "10:00:00",
                       "closeTime": "22:15:00",
                       "cutOffTime": 90,
                       "zoopCustomerDeliveryCharge": 15.25,
                       "zoopCustomerDeliveryChargeGstRate": 18,
                       "zoopCustomerDeliveryChargeGst": 2.75,
                       "cuisines": [
                           {
                               "cuisineId": 3,
                               "cuisineName": "North Indian"
                           },
                           {
                               "cuisineId": 11,
                               "cuisineName": "Chinese"
                           },
                           {
                               "cuisineId": 2,
                               "cuisineName": "Punjabi"
                           },
                           {
                               "cuisineId": 19,
                               "cuisineName": "Fast Food"
                           },
                           {
                               "cuisineId": 8,
                               "cuisineName": "Andhra"
                           },
                           {
                               "cuisineId": 1,
                               "cuisineName": "South Indian"
                           }
                       ],
                       "recommendedItems": [
                           {
                               "itemId": 32237,
                               "outletId": 38,
                               "itemName": "Jain Special Thali (10 Plates)",
                               "itemDescription": "Paneer Veg (100g); Seasonal Veg (100g); Dal Fry (100g); Jeera Rice (150g); Butter Roti (3); Sweet (1); Salad; Pickle.",
                               "categoryId": 1,
                               "typeId": 2,
                               "cuisineId": 3,
                               "zoopPrice": 1300,
                               "basePrice": 1430,
                               "basePriceGstRate": 5,
                               "basePriceGst": 71.5,
                               "sellingPrice": 1501.5,
                               "deliveryCharge": null,
                               "deliveryChargeGstRate": null,
                               "deliveryChargeGst": null,
                               "image": "Jain_Maharaja_Thali.jpg",
                               "isFestiveItem": 0,
                               "festiveStartDate": null,
                               "festiveEndDate": null,
                               "isRecommended": 1,
                               "isBestSelling": 0,
                               "categoryType": "Veg",
                               "typeName": "Thalis",
                               "cuisineName": "North Indian",
                               "itemCount": 0,
                               "itemTimes": [
                                   {
                                       "timeId": 21214,
                                       "itemId": 32237,
                                       "startTime": "10:00:00",
                                       "endTime": "23:00:00"
                                   }
                               ]
                           }
                       ],
                       "items": [
                           {
                               "itemId": 32187,
                               "outletId": 38,
                               "itemName": "Afgani Rolls (2pc)",
                               "itemDescription": "Roll (2pc); Chutney",
                               "categoryId": 2,
                               "typeId": 10,
                               "cuisineId": 3,
                               "zoopPrice": 80,
                               "basePrice": 104,
                               "basePriceGstRate": 5,
                               "basePriceGst": 5.2,
                               "sellingPrice": 109.2,
                               "deliveryCharge": null,
                               "deliveryChargeGstRate": null,
                               "deliveryChargeGst": null,
                               "image": null,
                               "isFestiveItem": 0,
                               "festiveStartDate": null,
                               "festiveEndDate": null,
                               "isRecommended": 0,
                               "isBestSelling": 0,
                               "categoryType": "Non-Veg",
                               "typeName": "Non-Veg Starters",
                               "cuisineName": "North Indian",
                               "itemCount": 0,
                               "itemTimes": [
                                   {
                                       "timeId": 21164,
                                       "itemId": 32187,
                                       "startTime": "10:00:00",
                                       "endTime": "23:00:00"
                                   }
                               ]
                           }
                       ]
                   }
               ]
           },
         ...
       ]
   }
}


```

Authentication Error :

```
{
    "status": false,
    "code": 401,
    "message": "",
    "error": "You are not authorised."
}
```

**- API for getting list of serving stations with outlets and their menus (search by PNR):**

**https://&lt;zoop-base-url>/search/pnr**

Example :
https://partners.staging.zoopindia.in/search/pnr

Request Method :
POST

Parameters :
valid string of length 10 (PNR)

**Request Body:**

{searchString : 6537801189}

**Response :**

```
{
    "status": true,
    "code": 200,
    "message": "Success.",
    "error": "",
    "data": {
        "seatInfo": {
            "coach": "B1",
            "berth": "13",
            "noOfSeats": 2
        },
        "passengerInfo": [
            {
                "currentCoach": "B1",
                "currentBerthNo": "13"
            },
            {
                "currentCoach": "B1",
                "currentBerthNo": "14"
            }
        ],
        "trainDetails": {
            "trainId": 1051,
            "trainNumber": "12926",
            "trainName": "PASCHIM EXPRESS",
            "status": 1
        },
        "trainRoutes": [
            {
                "trainId": 1051,
                "stationId": 1842,
                "arrivalTime": "19:10:00",
                "departureTime": "19:15:00",
                "haltTime": "00:05:00",
                "stationCode": "MTJ",
                "stationName": "MATHURA JN",
                "stationImage": "https://zoop.s3.amazonaws.com/station/MTJ.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T101908Z&X-Amz-Expires=30000&X-Amz-Signature=787eccda0a38469499f8d8c9c2ec33b6befd0d0dd877663dab431c63bffc8afd&X-Amz-SignedHeaders=host",
                "expectedTime": "19:10:00",
                "isVisible": true,
                "arrival": "19:05",
                "departure": "19:10",
                "halt": "05:00",
                "arrDate": "2019-12-06",
                "delayArrival": null,
                "schArrivalTime": "19:05",
                "schArrivalDate": "2019-12-06",
                "outlets": [
                    {
                        "outletId": 38,
                        "outletName": "Wah Ji Wah",
                        "isPureVeg": 1,
                        "outletRating": 4.6,
                        "outletImage": "https://zoop.s3.amazonaws.com/outlets-images/251.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T101908Z&X-Amz-Expires=30000&X-Amz-Signature=e4a6676bf07b089502779d8a22677d9d6bec82b7b87414aa0bba1278bf879ea0&X-Amz-SignedHeaders=host",
                        "weeklyOff": "noOff",
                        "gstin": "09AABPF1372P1ZJ",
                        "zoopStatus": 1,
                        "fssaiNo": "22717811000050",
                        "fssaiExpiryDate": "2022-04-26T18:30:00.000Z",
                        "minimumOrderValue": 125,
                        "openTime": "10:00:00",
                        "closeTime": "22:15:00",
                        "cutOffTime": 90,
                        "zoopCustomerDeliveryCharge": 15.25,
                        "zoopCustomerDeliveryChargeGstRate": 18,
                        "zoopCustomerDeliveryChargeGst": 2.75,
                        "cuisines": [
                            {
                                "cuisineId": 3,
                                "cuisineName": "North Indian"
                            },
                            {
                                "cuisineId": 11,
                                "cuisineName": "Chinese"
                            },
                            {
                                "cuisineId": 2,
                                "cuisineName": "Punjabi"
                            },
                            {
                                "cuisineId": 19,
                                "cuisineName": "Fast Food"
                            },
                            {
                                "cuisineId": 8,
                                "cuisineName": "Andhra"
                            },
                            {
                                "cuisineId": 1,
                                "cuisineName": "South Indian"
                            }
                        ],
                        "recommendedItems": [
                            {
                                "itemId": 32240,
                                "outletId": 38,
                                "itemName": "Wah Ji Wah Thali",
                                "itemDescription": "Paneer Veg (100g); Dal Fry (100g); Soya Tawa Tikka (100g); Veg Biryani (150g); Tawa Butter Roti (4); Sweet (1); Pickle; Papad.",
                                "categoryId": 1,
                                "typeId": 2,
                                "cuisineId": 3,
                                "zoopPrice": 180,
                                "basePrice": 234,
                                "basePriceGstRate": 5,
                                "basePriceGst": 11.7,
                                "sellingPrice": 245.7,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": null,
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 1,
                                "isBestSelling": 0,
                                "categoryType": "Veg",
                                "typeName": "Thalis",
                                "cuisineName": "North Indian",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 21217,
                                        "itemId": 32240,
                                        "startTime": "10:00:00",
                                        "endTime": "23:00:00"
                                    }
                                ]
                            },
                        ],
                        "items": [
                            {
                                "itemId": 32187,
                                "outletId": 38,
                                "itemName": "Afgani Rolls (2pc)",
                                "itemDescription": "Roll (2pc); Chutney",
                                "categoryId": 2,
                                "typeId": 10,
                                "cuisineId": 3,
                                "zoopPrice": 80,
                                "basePrice": 104,
                                "basePriceGstRate": 5,
                                "basePriceGst": 5.2,
                                "sellingPrice": 109.2,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": null,
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 0,
                                "isBestSelling": 0,
                                "categoryType": "Non-Veg",
                                "typeName": "Non-Veg Starters",
                                "cuisineName": "North Indian",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 21164,
                                        "itemId": 32187,
                                        "startTime": "10:00:00",
                                        "endTime": "23:00:00"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "outletId": 98,
                        "outletName": "Tulsi Family Restaurant",
                        "isPureVeg": 1,
                        "outletRating": 4.4,
                        "outletImage": "https://zoop.s3.amazonaws.com/outlets-images/378.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T101908Z&X-Amz-Expires=30000&X-Amz-Signature=5b1ab1d86346b144cf7adf3aae725f754640f6f54ffe6b2710332b1b3b80638e&X-Amz-SignedHeaders=host",
                        "weeklyOff": "noOff",
                        "gstin": "09AJOPG8830D2Z1",
                        "zoopStatus": 1,
                        "fssaiNo": "22718808000012",
                        "fssaiExpiryDate": "2020-01-16T18:30:00.000Z",
                        "minimumOrderValue": 70,
                        "openTime": "11:00:00",
                        "closeTime": "22:30:00",
                        "cutOffTime": 60,
                        "zoopCustomerDeliveryCharge": 15.25,
                        "zoopCustomerDeliveryChargeGstRate": 18,
                        "zoopCustomerDeliveryChargeGst": 2.75,
                        "cuisines": [
                            {
                                "cuisineId": 3,
                                "cuisineName": "North Indian"
                            },
                            {
                                "cuisineId": 2,
                                "cuisineName": "Punjabi"
                            },
                            {
                                "cuisineId": 11,
                                "cuisineName": "Chinese"
                            }
                        ],
                        "recommendedItems": [
                            {
                                "itemId": 35557,
                                "outletId": 98,
                                "itemName": "Jain Mini Thali (15 Plates)",
                                "itemDescription": "Mix Veg (100g); Dal Fry (100g); Rice (150g); Roti (3); Raita (100g); Salad; Pickle.",
                                "categoryId": 1,
                                "typeId": 2,
                                "cuisineId": 3,
                                "zoopPrice": 1200,
                                "basePrice": 1290,
                                "basePriceGstRate": 5,
                                "basePriceGst": 64.5,
                                "sellingPrice": 1354.5,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": "Jain_Standard_Thali.jpg",
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 1,
                                "isBestSelling": 0,
                                "categoryType": "Veg",
                                "typeName": "Thalis",
                                "cuisineName": "North Indian",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 24411,
                                        "itemId": 35557,
                                        "startTime": "10:00:00",
                                        "endTime": "22:00:00"
                                    }
                                ]
                            },
                        ],
                        "items": [
                            {
                                "itemId": 35557,
                                "outletId": 98,
                                "itemName": "Jain Mini Thali (15 Plates)",
                                "itemDescription": "Mix Veg (100g); Dal Fry (100g); Rice (150g); Roti (3); Raita (100g); Salad; Pickle.",
                                "categoryId": 1,
                                "typeId": 2,
                                "cuisineId": 3,
                                "zoopPrice": 1200,
                                "basePrice": 1290,
                                "basePriceGstRate": 5,
                                "basePriceGst": 64.5,
                                "sellingPrice": 1354.5,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": "Jain_Standard_Thali.jpg",
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 1,
                                "isBestSelling": 0,
                                "categoryType": "Veg",
                                "typeName": "Thalis",
                                "cuisineName": "North Indian",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 24411,
                                        "itemId": 35557,
                                        "startTime": "10:00:00",
                                        "endTime": "22:00:00"
                                    }
                                ]
                            },
                        ]
                    }
                ]
            },
            {
                "trainId": 1051,
                "stationId": 7318,
                "arrivalTime": "21:13:00",
                "departureTime": "21:15:00",
                "haltTime": "00:02:00",
                "stationCode": "GGC",
                "stationName": "GANGAPUR CITY",
                "stationImage": "https://zoop.s3.amazonaws.com/station/GGC.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T101908Z&X-Amz-Expires=30000&X-Amz-Signature=7932d081c3eabb4fd2d03b589d406dd5a346868369b0f49912a87bf61b7a4eec&X-Amz-SignedHeaders=host",
                "expectedTime": "21:13:00",
                "isVisible": true,
                "arrival": "21:13",
                "departure": "21:15",
                "halt": "02:00",
                "arrDate": "2019-12-06",
                "delayArrival": null,
                "schArrivalTime": "21:13",
                "schArrivalDate": "2019-12-06",
                "outlets": [
                    {
                        "outletId": 66,
                        "outletName": "Krishna Restorent",
                        "isPureVeg": 1,
                        "outletRating": 4.5,
                        "outletImage": "https://zoop.s3.amazonaws.com/outlets-images/324.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T101908Z&X-Amz-Expires=30000&X-Amz-Signature=778d270325ca1afeee1a397e5eae7ac70b87a2b178dc3718399f463d969fefaa&X-Amz-SignedHeaders=host",
                        "weeklyOff": "noOff",
                        "gstin": "08AMQPM4130R2ZB",
                        "zoopStatus": 1,
                        "fssaiNo": "12215038000116",
                        "fssaiExpiryDate": "2021-08-18T18:30:00.000Z",
                        "minimumOrderValue": 150,
                        "openTime": "09:00:00",
                        "closeTime": "22:00:00",
                        "cutOffTime": 75,
                        "zoopCustomerDeliveryCharge": null,
                        "zoopCustomerDeliveryChargeGstRate": null,
                        "zoopCustomerDeliveryChargeGst": null,
                        "cuisines": [
                            {
                                "cuisineId": 3,
                                "cuisineName": "North Indian"
                            },
                            {
                                "cuisineId": 19,
                                "cuisineName": "Fast Food"
                            },
                            {
                                "cuisineId": 11,
                                "cuisineName": "Chinese"
                            },
                            {
                                "cuisineId": 1,
                                "cuisineName": "South Indian"
                            }
                        ],
                        "recommendedItems": [
                            {
                                "itemId": 33884,
                                "outletId": 66,
                                "itemName": "Jain Special Thali (10 Plates)",
                                "itemDescription": "Paneer Veg (100g); Seasonal Veg (100g); Dal Fry (100g); Jeera Rice (150g); Butter Roti (4); Sweet (1); Salad; Pickle.",
                                "categoryId": 1,
                                "typeId": 2,
                                "cuisineId": 3,
                                "zoopPrice": 1400,
                                "basePrice": 1540,
                                "basePriceGstRate": 5,
                                "basePriceGst": 77,
                                "sellingPrice": 1617,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": "Jain_Maharaja_Thali.jpg",
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 1,
                                "isBestSelling": 0,
                                "categoryType": "Veg",
                                "typeName": "Thalis",
                                "cuisineName": "North Indian",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 22796,
                                        "itemId": 33884,
                                        "startTime": "10:00:00",
                                        "endTime": "22:00:00"
                                    }
                                ]
                            },
                            {
                                "itemId": 33883,
                                "outletId": 66,
                                "itemName": "Combo Veg Pulao n Raita",
                                "itemDescription": "Pulao (400g); Raita/Salad; Pickle",
                                "categoryId": 1,
                                "typeId": 3,
                                "cuisineId": 3,
                                "zoopPrice": 80,
                                "basePrice": 120,
                                "basePriceGstRate": 5,
                                "basePriceGst": 6,
                                "sellingPrice": 126,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": "Veg_Pulao.jpg",
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 1,
                                "isBestSelling": 0,
                                "categoryType": "Veg",
                                "typeName": "Combos",
                                "cuisineName": "North Indian",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 22795,
                                        "itemId": 33883,
                                        "startTime": "10:00:00",
                                        "endTime": "22:00:00"
                                    }
                                ]
                            }
                        ],
                        "items": [
                            {
                                "itemId": 33884,
                                "outletId": 66,
                                "itemName": "Jain Special Thali (10 Plates)",
                                "itemDescription": "Paneer Veg (100g); Seasonal Veg (100g); Dal Fry (100g); Jeera Rice (150g); Butter Roti (4); Sweet (1); Salad; Pickle.",
                                "categoryId": 1,
                                "typeId": 2,
                                "cuisineId": 3,
                                "zoopPrice": 1400,
                                "basePrice": 1540,
                                "basePriceGstRate": 5,
                                "basePriceGst": 77,
                                "sellingPrice": 1617,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": "Jain_Maharaja_Thali.jpg",
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 1,
                                "isBestSelling": 0,
                                "categoryType": "Veg",
                                "typeName": "Thalis",
                                "cuisineName": "North Indian",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 22796,
                                        "itemId": 33884,
                                        "startTime": "10:00:00",
                                        "endTime": "22:00:00"
                                    }
                                ]
                            },
                            {
                                "itemId": 33858,
                                "outletId": 66,
                                "itemName": "Pav Bhaji (2)",
                                "itemDescription": "Pav (2); Bhaji; Salad",
                                "categoryId": 1,
                                "typeId": 9,
                                "cuisineId": 19,
                                "zoopPrice": 60,
                                "basePrice": 95,
                                "basePriceGstRate": 5,
                                "basePriceGst": 4.75,
                                "sellingPrice": 99.75,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": "Pao_Bhaji.jpg",
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 0,
                                "isBestSelling": 0,
                                "categoryType": "Veg",
                                "typeName": "Veg Starters",
                                "cuisineName": "Fast Food",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 22770,
                                        "itemId": 33858,
                                        "startTime": "08:00:00",
                                        "endTime": "22:00:00"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "trainId": 1051,
                "stationId": 7372,
                "arrivalTime": "22:00:00",
                "departureTime": "22:05:00",
                "haltTime": "00:05:00",
                "stationCode": "SWM",
                "stationName": "SAWAI MADHOPUR",
                "stationImage": "https://zoop.s3.amazonaws.com/station/SWM.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T101908Z&X-Amz-Expires=30000&X-Amz-Signature=7da8abe9484582058f97e5fe7444ede6367c9d0883ef022f9fb1b4d10fe0a846&X-Amz-SignedHeaders=host",
                "expectedTime": "22:00:00",
                "isVisible": true,
                "arrival": "22:00",
                "departure": "22:02",
                "halt": "02:00",
                "arrDate": "2019-12-06",
                "delayArrival": null,
                "schArrivalTime": "22:00",
                "schArrivalDate": "2019-12-06",
                "outlets": [
                    {
                        "outletId": 67,
                        "outletName": "Sharma Foods",
                        "isPureVeg": 1,
                        "outletRating": 4.5,
                        "outletImage": "https://zoop.s3.amazonaws.com/outlets-images/Restaurant.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T101908Z&X-Amz-Expires=30000&X-Amz-Signature=a2b3312554520cc2765593b08d605e7afcd33292d0ac7eefe99c84cab4b0352e&X-Amz-SignedHeaders=host",
                        "weeklyOff": "noOff",
                        "gstin": "08CCHPS2620F1ZO",
                        "zoopStatus": 1,
                        "fssaiNo": "22216067000346",
                        "fssaiExpiryDate": "2024-07-26T18:30:00.000Z",
                        "minimumOrderValue": 99,
                        "openTime": "08:00:00",
                        "closeTime": "22:15:00",
                        "cutOffTime": 55,
                        "zoopCustomerDeliveryCharge": null,
                        "zoopCustomerDeliveryChargeGstRate": null,
                        "zoopCustomerDeliveryChargeGst": null,
                        "cuisines": [
                            {
                                "cuisineId": 3,
                                "cuisineName": "North Indian"
                            },
                            {
                                "cuisineId": 11,
                                "cuisineName": "Chinese"
                            },
                            {
                                "cuisineId": 2,
                                "cuisineName": "Punjabi"
                            },
                            {
                                "cuisineId": 19,
                                "cuisineName": "Fast Food"
                            }
                        ],
                        "recommendedItems": [
                            {
                                "itemId": 33935,
                                "outletId": 67,
                                "itemName": "Jain Special Thali (10 Plates)",
                                "itemDescription": "Paneer Veg (100g); Seasonal Veg (100g); Dal Fry (100g); Jeera Rice (150g); Butter Roti (3); Sweet (1); Salad; Pickle.",
                                "categoryId": 1,
                                "typeId": 2,
                                "cuisineId": 3,
                                "zoopPrice": 1250,
                                "basePrice": 1370,
                                "basePriceGstRate": 5,
                                "basePriceGst": 68.5,
                                "sellingPrice": 1438.5,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": "Jain_Maharaja_Thali.jpg",
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 1,
                                "isBestSelling": 0,
                                "categoryType": "Veg",
                                "typeName": "Thalis",
                                "cuisineName": "North Indian",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 22847,
                                        "itemId": 33935,
                                        "startTime": "08:00:00",
                                        "endTime": "23:00:00"
                                    }
                                ]
                            },
                        ],
                        "items": [
                            {
                                "itemId": 33928,
                                "outletId": 67,
                                "itemName": "Combo Veg Pulao n Raita",
                                "itemDescription": "Pulao (300g); Raita/Salad; Pickle",
                                "categoryId": 1,
                                "typeId": 3,
                                "cuisineId": 3,
                                "zoopPrice": 74,
                                "basePrice": 105,
                                "basePriceGstRate": 5,
                                "basePriceGst": 5.25,
                                "sellingPrice": 110.25,
                                "deliveryCharge": null,
                                "deliveryChargeGstRate": null,
                                "deliveryChargeGst": null,
                                "image": "Veg_Pulao.jpg",
                                "isFestiveItem": 0,
                                "festiveStartDate": null,
                                "festiveEndDate": null,
                                "isRecommended": 1,
                                "isBestSelling": 0,
                                "categoryType": "Veg",
                                "typeName": "Combos",
                                "cuisineName": "North Indian",
                                "itemCount": 0,
                                "itemTimes": [
                                    {
                                        "timeId": 22840,
                                        "itemId": 33928,
                                        "startTime": "08:00:00",
                                        "endTime": "23:00:00"
                                    }
                                ]
                            },
                        ]
                    }
                ]
            }
        ]
    }
}


```

Authentication Error :

```
{
    "status": false,
    "code": 401,
    "message": "",
    "error": "You are not authorised."
}
```

**- API for validating the PNR after search by train :**

**https://&lt;zoop-base-url>/validate-search/pnr**

Example :
https://partners.staging.zoopindia.in/validate-search/pnr

Request Method :
POST

Parameters :
Valid PNR string of length 10, trainId, trainNumber, stationId, stationCode, outletId, deliveryDate

**Request Body :**

{
    "searchString": "8227449358",
    "trainId": 2208,
    "trainNumber": "18502",
    "stationId": 7,
    "stationCode": "BD",
    "outletId": 55,
    "deliveryDate": "2019-12-9"
}

**Response :**

```
{
   "status": true,
   "code": 200,
   "message": "Success.",
   "error": "",
   "data": {
       "seatInfo": {
           "coach": "B1",
           "berth": "13",
           "noOfSeats": 2
       },
       "passengerInfo": [
           {
               "currentCoach": "B1",
               "currentBerthNo": "13"
           },
           {
               "currentCoach": "B1",
               "currentBerthNo": "14"
           }
       ],
       "trainDetails": {
           "trainId": 1051,
           "trainNumber": "12926",
           "trainName": "PASCHIM EXPRESS",
           "status": 1
       },
       "trainRoutes": [
           {
               "trainId": 1051,
               "stationId": 1842,
               "arrivalTime": "19:10:00",
               "departureTime": "19:15:00",
               "haltTime": "00:05:00",
               "stationCode": "MTJ",
               "stationName": "MATHURA JN",
               "stationImage": "https://zoop.s3.amazonaws.com/station/MTJ.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T054233Z&X-Amz-Expires=30000&X-Amz-Signature=d049e86812c9c3a3dd05d65ce52eda9879f01d29c4b0dbcdbd27c1e2d34c5caa&X-Amz-SignedHeaders=host",
               "expectedTime": "19:10:00",
               "isVisible": true,
               "arrival": "19:05",
               "departure": "19:10",
               "halt": "05:00",
               "arrDate": "2019-12-06",
               "delayArrival": null,
               "schArrivalTime": "19:05",
               "schArrivalDate": "2019-12-06",
               "outlets": [
                   {
                       "outletId": 38,
                       "outletName": "Wah Ji Wah",
                       "isPureVeg": 1,
                       "outletRating": 4.6,
                       "outletImage": "https://zoop.s3.amazonaws.com/outlets-images/251.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIATQPRMEK7GDQDWG7G%2F20191206%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20191206T054233Z&X-Amz-Expires=30000&X-Amz-Signature=21bf07a19b3529b3c2726f9cf91fa569ae5d867c4dda2692543ef568cf424789&X-Amz-SignedHeaders=host",
                       "weeklyOff": "noOff",
                       "gstin": "09AABPF1372P1ZJ",
                       "zoopStatus": 1,
                       "fssaiNo": "22717811000050",
                       "fssaiExpiryDate": "2022-04-26T18:30:00.000Z",
                       "minimumOrderValue": 125,
                       "openTime": "10:00:00",
                       "closeTime": "22:15:00",
                       "cutOffTime": 90,
                       "zoopCustomerDeliveryCharge": 15.25,
                       "zoopCustomerDeliveryChargeGstRate": 18,
                       "zoopCustomerDeliveryChargeGst": 2.75,
                       "cuisines": [
                           {
                               "cuisineId": 3,
                               "cuisineName": "North Indian"
                           },
                           {
                               "cuisineId": 11,
                               "cuisineName": "Chinese"
                           },
                           {
                               "cuisineId": 2,
                               "cuisineName": "Punjabi"
                           },
                           {
                               "cuisineId": 19,
                               "cuisineName": "Fast Food"
                           },
                           {
                               "cuisineId": 8,
                               "cuisineName": "Andhra"
                           },
                           {
                               "cuisineId": 1,
                               "cuisineName": "South Indian"
                           }
                       ],
                       "recommendedItems": [
                           {
                               "itemId": 32237,
                               "outletId": 38,
                               "itemName": "Jain Special Thali (10 Plates)",
                               "itemDescription": "Paneer Veg (100g); Seasonal Veg (100g); Dal Fry (100g); Jeera Rice (150g); Butter Roti (3); Sweet (1); Salad; Pickle.",
                               "categoryId": 1,
                               "typeId": 2,
                               "cuisineId": 3,
                               "zoopPrice": 1300,
                               "basePrice": 1430,
                               "basePriceGstRate": 5,
                               "basePriceGst": 71.5,
                               "sellingPrice": 1501.5,
                               "deliveryCharge": null,
                               "deliveryChargeGstRate": null,
                               "deliveryChargeGst": null,
                               "image": "Jain_Maharaja_Thali.jpg",
                               "isFestiveItem": 0,
                               "festiveStartDate": null,
                               "festiveEndDate": null,
                               "isRecommended": 1,
                               "isBestSelling": 0,
                               "categoryType": "Veg",
                               "typeName": "Thalis",
                               "cuisineName": "North Indian",
                               "itemCount": 0,
                               "itemTimes": [
                                   {
                                       "timeId": 21214,
                                       "itemId": 32237,
                                       "startTime": "10:00:00",
                                       "endTime": "23:00:00"
                                   }
                               ]
                           }
                       ],
                       "items": [
                           {
                               "itemId": 32188,
                               "outletId": 38,
                               "itemName": "Soya Lolypop (4pc)",
                               "itemDescription": "(4pc)",
                               "categoryId": 1,
                               "typeId": 9,
                               "cuisineId": 3,
                               "zoopPrice": 150,
                               "basePrice": 195,
                               "basePriceGstRate": 5,
                               "basePriceGst": 9.75,
                               "sellingPrice": 204.75,
                               "deliveryCharge": null,
                               "deliveryChargeGstRate": null,
                               "deliveryChargeGst": null,
                               "image": null,
                               "isFestiveItem": 0,
                               "festiveStartDate": null,
                               "festiveEndDate": null,
                               "isRecommended": 0,
                               "isBestSelling": 0,
                               "categoryType": "Veg",
                               "typeName": "Veg Starters",
                               "cuisineName": "North Indian",
                               "itemCount": 0,
                               "itemTimes": [
                                   {
                                       "timeId": 21165,
                                       "itemId": 32188,
                                       "startTime": "10:00:00",
                                       "endTime": "23:00:00"
                                   }
                               ]
                           }
                       ]
                   }
               ]
           }
       ]
   }
}


```

Authentication Error :

```
{
    "status": false,
    "code": 401,
    "message": "",
    "error": "You are not authorised."
}
```

**API for creating new order :**

_https://<zoop-base-url>/orders_

**Example :**

https://partners.staging.zoopindia.in/orders

**Request Method :**

POST

**Parameters :**

N/A

**Request Body :**

```
{	
	outletId : 10,
	source : 'PartnersName',
	billDetail : {
		totalAmount : 100,
		deliveryCharge : 0,
		deliveryChargeGst : 0
		deliveryChargeGstRate : 0
		discount : 0
		couponId : 0
		couponCode : 0						 
		couponValue : 0
		walletAmount : 0
		gst : 5
		totalPayableAmount : 105
	},
	passengerDetail : {
		pnr : '8987656765',
		berth : '54',
		coach : 'B2',
		trainId : 12137,
		stationId : 155,
		passengerName : 'Rakesh',
		passengerMobile : '9988998899',
		passengeAlternateMobile : '8899889988',
		passengerEmail : 'partners@gmail.com',
		suggestions : 'Please send hot food.',
		deliveryDate : '2021-12-04',
		deliveryTime : '14:30:00',
		passengerSeatInfo : {}
	},
	paymentDetail : {
		paymentTypeId : 1 // 1 - COD, 2- PPD
	},
	items : [
		{
			itemId : 31051,
			itemName : 'Yellow Dal Tadka',
			itemDescription : '300g',
			quantity : 2,
			categoryId : 1,
			typeId : 19,
			cuisineId : 3,
			zoopPrice : 70.00,
			basePrice : 105.00,
			basePriceGstRate : 5,
			basePriceGst : 5.25,
			sellingPrice : 110.25,
			deliveryCharge : 0,
			deliveryChargeGstRate : 0,
			deliveryChargeGst : 0
		}
	]
}
```
Response :

```
{
    orderId : 12345678,
    zoopTransactionNo : 'ZO114566733'
}
```

**API for getting list of cuisines:**

https://<zoop-base-url>/orders/irctc-status/{orderId}

**Example :**

https://partners.staging.zoopindia.in/orders/irctc-status/12345678

**Request Method :**

GET

**Parameters :**

N/A

**Request Body:**

N/A

**Response :**

```
{
    irctcOrderId : 31638452
}

```





